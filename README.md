# Drainer un noeud Kubernetes en toute sécurité

------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______ 

# Objectifs 

Aperçu des notions couvertes :

1. Notion de "drainage" de noeud ?
2. Le processus de "drainage" d'un nœud
3. Ignorer les DaemonSets
4. Déverrouiller un nœud
5. Démonstration pratique du "drainage" sécurisée d'un nœud Kubernetes

# 1. Qu'est-ce que le drainage ?
Lors de la maintenance des clusters Kubernetes, il peut être nécessaire de retirer temporairement un nœud Kubernetes du service. L'objectif est que toutes les applications continuent de fonctionner même si un nœud est retiré pour maintenance. Le processus de drainage d'un nœud consiste à arrêter de manière gracieuse les conteneurs en cours d'exécution sur le nœud et à les déplacer potentiellement vers un autre nœud pour éviter toute interruption de service. Cela se fait facilement avec la commande kubectl drain suivie du nom du nœud.

# 2. Le processus de drainage d'un nœud
Le drainage d'un nœud Kubernetes entraîne la terminaison gracieuse des conteneurs sur le nœud, avec la possibilité de les déplacer vers d'autres nœuds pour maintenir le service. La commande kubectl drain permet de réaliser cette opération. Toutefois, certains paramètres doivent être pris en compte, notamment l'ignorance des DaemonSets.

# 3. Ignorer les DaemonSets
Lors du drainage d'un nœud, il peut être nécessaire d'ignorer les DaemonSets. Les DaemonSets sont des pods liés spécifiquement à chaque nœud et ne peuvent pas être facilement déplacés. Si des pods DaemonSet s'exécutent sur un nœud et que la commande kubectl drain est utilisée sans paramètre supplémentaire, un message d'erreur indique que le nœud ne peut pas être drainé en raison de ces DaemonSets. L'ajout de l'option --ignore-daemonsets à la commande kubectl drain permet de contourner ce problème.

# 4. Déverrouiller un nœud
Une fois la maintenance terminée, il est nécessaire de permettre à Kubernetes de redémarrer les conteneurs sur le nœud drainé. Cela se fait avec la commande kubectl uncordon, qui indique à Kubernetes que le nœud est prêt à exécuter des pods et des conteneurs à nouveau.

# 5. Démonstration pratique du drainage sécurisé d'un nœud Kubernetes

[Retrouvez les étapes de cette démonstration en suivant ce lien](https://gitlab.com/CarlinFongang-Labs/kubernetes/gestion-des-clusters/lab-drainer-un-noeud-k8s.git)

La démonstration pratique montre comment drainer un nœud en utilisant les commandes appropriées et en tenant compte des paramètres nécessaires pour gérer les erreurs liées aux pods non gérés et aux DaemonSets. Une fois le nœud drainé et la maintenance terminée, le nœud est déverrouillé et prêt à reprendre les charges de travail.

# Conclusion
Cette leçon a expliqué ce qu'est le drainage d'un nœud, comment l'exécuter en utilisant kubectl drain, l'importance d'ignorer les DaemonSets avec le paramètre --ignore-daemonsets, et comment déverrouiller un nœud avec kubectl uncordon. La démonstration pratique a illustré ces concepts et montré comment gérer les pods et les déploiements lors du drainage d'un nœud Kubernetes.


# Reférences

https://kubernetes.io/docs/tasks/administer-cluster/safely-drain-node/

